package com.timetable.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.google.gson.Gson;
import com.timetable.entity.TeacherEntity;
import com.timetable.resources.Factory;
import com.timetable.services.TeacherService;

@Controller
@RequestMapping(path = "/teacher")
public class TeacherController {

	@Autowired
	private TeacherService teacherService = Factory.createTeacherService();

	@GetMapping(path = "/getAllTeachers")
	public ResponseEntity<?> getAllTeachers() {
		ResponseEntity<?> returnValue = null;
		try {
			List<TeacherEntity> finalList = new ArrayList<>();
			finalList = teacherService.getAllTeacher();
			returnValue = ResponseEntity.ok(finalList);
		} catch (Exception e) {
			if (e.getMessage().contains("dao")) {
				returnValue = ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).body("Error in Hibernate");
			} else {
				returnValue = ResponseEntity.status(HttpStatus.BAD_REQUEST).body("General Error");
			}
		}
		return returnValue;
	}

	@PostMapping(path = "/addTeacher")
	public ResponseEntity<?> getAllTeachers(@RequestBody String dataRecieved) {
		ResponseEntity<?> returnValue = null;
		Gson gson = new Gson();
		try {
			TeacherEntity te = gson.fromJson(dataRecieved, TeacherEntity.class);
			TeacherEntity te1 = new TeacherEntity();
			te1 = teacherService.addNeWTeacher(te);
			returnValue = ResponseEntity.ok(te1);
		} catch (Exception e) {
			if (e.getMessage().contains("dao")) {
				returnValue = ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).body("Error in Hibernate");
			} else {
				returnValue = ResponseEntity.status(HttpStatus.BAD_REQUEST).body("General Error");
			}
		}
		return returnValue;
	}
}
