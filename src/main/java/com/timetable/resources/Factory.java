package com.timetable.resources;

import com.timetable.dao.TeacherDAO;
import com.timetable.dao.TeacherDAOImpl;
import com.timetable.services.TeacherService;
import com.timetable.services.TeacherServiceImpl;

public class Factory {
	public static TeacherDAO createTeacherDao() {
		return new TeacherDAOImpl();
	}

	public static TeacherService createTeacherService() {
		return new TeacherServiceImpl();
	}
}
