package com.timetable.repository;

import org.springframework.data.repository.CrudRepository;

import com.timetable.entity.TeacherEntity;

public interface TeacherRepository extends CrudRepository<TeacherEntity, Integer> {

}
