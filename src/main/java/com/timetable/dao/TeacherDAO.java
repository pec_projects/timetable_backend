package com.timetable.dao;

import java.util.List;

import com.timetable.entity.TeacherEntity;

public interface TeacherDAO {

	public List<TeacherEntity> getAllTeacher() throws Exception;
	
	public TeacherEntity addNeWTeacher(TeacherEntity te) throws Exception;

}
