package com.timetable.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.timetable.entity.TeacherEntity;
import com.timetable.repository.TeacherRepository;
import com.timetable.resources.Factory;

@Repository
public class TeacherDAOImpl implements TeacherDAO {

	@Autowired
	private TeacherRepository teacherRepository;

	@Override
	public List<TeacherEntity> getAllTeacher() throws Exception {
		List<TeacherEntity> finalList = new ArrayList<>();
		try {
			Iterable<TeacherEntity> tempList = new ArrayList<>();
			tempList = teacherRepository.findAll();
			for (TeacherEntity te : tempList) {
				finalList.add(te);
			}
		} catch (HibernateException he) {
			Logger logger = LogManager.getLogger();
			logger.error("Error in Hibernate: " + he.getMessage(), he);
			throw new Exception("daoTechnicalError");
		} catch (Exception e) {
			Logger logger = LogManager.getLogger();
			logger.error("Error in DAO: " + e.getMessage(), e);
			throw e;
		}
		return finalList;
	}

	@Override
	public TeacherEntity addNeWTeacher(TeacherEntity te) throws Exception {
		teacherRepository.save(te);
		return te;
	}

}
