package com.timetable.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.timetable.dao.TeacherDAO;
import com.timetable.entity.TeacherEntity;
import com.timetable.resources.Factory;

@Service
public class TeacherServiceImpl implements TeacherService {

	@Autowired
	private TeacherDAO teacherDAO = Factory.createTeacherDao();

	@Override
	public List<TeacherEntity> getAllTeacher() throws Exception {
		List<TeacherEntity> finalList = new ArrayList<>();
		try {
			finalList = teacherDAO.getAllTeacher();
		} catch (Exception e) {
			throw e;
		}
		return finalList;
	}

	@Override
	public TeacherEntity addNeWTeacher(TeacherEntity te) throws Exception {
		TeacherEntity te1 = new TeacherEntity();
		te1 = teacherDAO.addNeWTeacher(te);
		return te1;
	}

}
