package com.timetable.services;

import java.util.List;

import com.timetable.entity.TeacherEntity;

public interface TeacherService {

	public List<TeacherEntity> getAllTeacher() throws Exception;
	
	public TeacherEntity addNeWTeacher(TeacherEntity te) throws Exception;

}
